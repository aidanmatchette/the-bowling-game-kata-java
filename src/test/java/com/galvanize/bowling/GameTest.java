package com.galvanize.bowling;

import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GameTest {

    private Game game ;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void canScoreGutterGame() {
        game.roll(0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0);
        Assertions.assertEquals(game.score(), 0);
    }

    @Test
    public void canScoreGameOfOnes() {
        game.roll(1,1, 1,1, 1,1, 1,1, 1,1, 1,1, 1,1, 1,1, 1,1, 1,1);
        Assertions.assertEquals(game.score(), 20);
    }

    @Test
    public void canScoreSpareFollowedByThree() {
        game.roll(5,5, 3,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0);
        Assertions.assertEquals(game.score(), 13);
    }

    @Test
    public void canScoreStrikeFollowedByThreeThenThree() {
        game.roll(10,0, 3,3, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0);
        Assertions.assertEquals(game.score(), 22);
    }

    @Test
    public void canScorePerfectGame() {
        game.roll(10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10);
        Assertions.assertEquals(game.score(), 300);
    }

}
